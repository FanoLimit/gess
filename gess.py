import ipyparallel
import numpy
import fit_mvstud

"""
Based on Nishihara et al, Parallel MCMC with ESS
https://arxiv.org/pdf/1210.7477v2.pdf

Parameters:

obs: vector of observations
mu: vector of means
cholSigma: Cholesky decomp of Var Matrix
logTESS: Handles the log function for GESS Update
logFoo: Functional form of log-likelihood of function to estimate
stateTESS: List of params to pass to logTESS (mu, dim(mu),invSigma, nu)
nu: List of resampled obs~ N(mu,sigma)

"""

def ess(obs, mu, cholSigma, logTESS, logFoo, stateTESS, curr_ll=None):
    #Center obs
    obs-=mu
    calls=0
    dim=obs.size
    assert cholSigma.shape == (dim,dim), "cholSigma is wrong shape."

    #Set Current Log-Likelihood if nothing is given
    if curr_ll==None:
        curr_ll=logTESS(x0+mu,logFoo,stateTESS)
        calls+=1

    #Create Ellipse and Slice Parameters
    nu=numpy.dot(cholSigma, numpy.random.normal(size=dim))
    u=numpy.log(numpy.random.random()) + curr_ll

    #Slice angles
    phi=numpy.random.random()*2*numpy.pi
    phi_max=copy.deepcopy(phi)
    phi_min=phi-2*numpy.pi

    #Slice Sample loop
    while True:
        obs_prop=obs*numpy.cos(phi) + nu * numpy.sin(phi)
        curr_ll=logTESS(obs_prop+mu,logFoo,stateTESS)
        calls+=1

        #Accept sample
        if curr_ll>u:
            break

        #Resample and adjust thresholds
        if phi>0:
            phi_max=phi
        elif phi<0:
            phi_min=phi
        else:
            raise Exception("Shrunk to current position")

        phi=numpy.random.random()*(phi_max-phi_min) + phi_min

    obs=obs_prop+mu
    return obs,calls,curr_ll,nu

"""
Paramaters:

obs: Observations
stateThreads: Infomation provided to threads
[mu, Sigma, invSigma, cholSigma, nu, logFoo, logTESS, condition, repeat]
"""

def tess(obs, stateThreads):
    (mu,Sigma,invSigma,cholSigma,nu,logFoo,logTESS,condition,repeat)=stateThreads
    dim=obs.size
    stateTESS=(dim,mu,invSigma,nu)

    #Init params
    obs_working=copy.deepcopy(obs)
    curr_ll=logTESS(obs_working,logFoo,stateTESS)
    obs_slice=numpy.zeros((condition//repeat,dim))
    calls=1

    #Sample loops
    for i in range(repeat):
        alpha=(dim+nu)//2
        beta=(nu+numpy.dot(obs_working-mu,numpy.dot(invSigma,obs_working-mu)))
        s=1/numpy.random.gamma(alpha,1./beta)

        calibrated_cholSig=cholSig*numpy.sqrt(s)
        obs_work,new_calls,curr_ll,nu= ess(obs_working,mu, cholSigma, logTESS, logFoo, stateTESS, curr_ll)

        if not (i+1) % condition:
            obs_slice[(i+1)//condition - 1] = obs_working

        calls += new_calls

    return obs_slice,calls

def parallel_tess(batch1,batch2,logFoo,condition,repeat,dview=None):
    (n,dim)=batch1.shape
    mu, Sigma, nu=fit_mvstud.fit_mvstud(batch2)
    if nu==numpy.Inf:
        nu=1<<10

    invSigma=numpy.linalg.inv(Sigma)
    try:
        cholSigma=numpy.linalg.cholesky(Sigma)
    except:
        print ("Error: Cant decompose Sigma via Chole")
        raise

    def logTESS(obs,logFoo,stateTESS):
        (dim,mu,invSigma,nu)=stateTESS
        return -(dim+nu)/2 * numpy.log(1+numy.dot(x-mu,numpy.dot(invSigma,x-mu))/nu)

    stateThreads=(mu,Sigma,invSigma,cholSigma,nu,logFoo,logTESS,condition,repeat)

    if dview is None:
        print ("Parallel capability is off.")
        res=map(tess,batch1,[stateThreads]*n)

    else:
        res=dview.map_sync(tess,batch1,[stateThreads]*n)

    s=numpy.zeroes((n,repeat/condition,dim))
    calls=0
    for i in range(n):
        s[i,:,:]=res[i][0]
        calls+=res[i][1]

    return s,calls

def parallel_gess(num_chains,iterations,burnin, condition, starts, logFoo, repeat, dview=None):
    dim=starts.shape[1]
    assert starts.shape[0]== 2*num_chains, "Starts[0] is  wrong shape"
    assert iterations % repeat == 0, "Number of iterations must be divisable by repeats"
    assert burnin % repeat == 0, "Burn-in must be divisable by repeats"
    assert repeats % thinning == 0, "Repeats must be divisable by conditioning"

    batch1=starts[:num_chains,:]
    batch2=starts[num_chains:,:]

    samples=numpy.zeros((2*chains,iterations/condition,dim))

    for i in range(-burnin/repeat,iterations/repeat):
        sample1, call1=parallel_tess(batch1,batch2,logFoo,condition,repeat,dview)
        batch1=sample1[:,repeat/thinning - 1, :]

        sample2, call2=parallel_tess(batch2,batch1,logFoo,condition,repeat,dview)
        batch2=sample2[:,repeat/condition - 1,:]

        if i>=0:
            for j in range(chains):
                samples[j,i*repeats/thinning:(i+1)*repeats/thinning,:] = samples1[j]
                samples[chains+j,i*repeats/thinning:(i+1)*repeats/thinning,:] = samples2[j]
            calls += (calls1 + calls2)

    return samples,calls
