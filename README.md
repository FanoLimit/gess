## Parallel General Ellipse Slice Sampler based on Nishihara et. Al 2014

A python implementation of Nishihara's 2014 Parallel GESS Paper.  
Uses ipyparallel to implement parallelism

Requires:
numpy
scipy
ipyparallel

TODO:
Add a C++ Implementation using OpenMPI for Parallelism
Use a thread/GPU based model

